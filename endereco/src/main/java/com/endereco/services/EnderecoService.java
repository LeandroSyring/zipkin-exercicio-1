package com.endereco.services;

import com.endereco.client.CepClient;
import com.endereco.client.CepDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService {

    @Autowired
    private CepClient cepClient;

    public CepDTO buscarCep(String cep){
        return cepClient.buscarCep(cep);
    }
}
