package com.endereco.controllers;

import com.endereco.client.CepDTO;
import com.endereco.mapper.EnderecoMapper;
import com.endereco.models.Endereco;
import com.endereco.services.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/endereco")
public class EnderecoController {

    @Autowired
    private EnderecoService enderecoService;

    @GetMapping("/{usuario}/{cep}")
    public ResponseEntity<Endereco> buscarEndereco(@PathVariable String usuario, @PathVariable String cep){

        Endereco endereco = EnderecoMapper.fromCepDTO(enderecoService.buscarCep(cep));

        endereco.setUsuario(usuario);

        return ResponseEntity.status(200).body(endereco);
    }
}
