package com.endereco.mapper;

import com.endereco.client.CepDTO;
import com.endereco.models.Endereco;

public class EnderecoMapper {

    public static Endereco fromCepDTO(CepDTO cepDTO){
        Endereco endereco = new Endereco();
        endereco.setCep(cepDTO.getCep());
        endereco.setLogradouro(cepDTO.getLogradouro());
        endereco.setComplemento(cepDTO.getComplemento());
        endereco.setBairro(cepDTO.getBairro());
        endereco.setLocalidade(cepDTO.getLocalidade());
        endereco.setUf(cepDTO.getUf());

        return endereco;
    }
}
