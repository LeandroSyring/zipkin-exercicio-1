package com.cep.services;

import com.cep.client.CepClient;
import com.cep.mappers.CepDTOMapper;
import com.cep.models.Cep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepClient cepClient;

    @NewSpan(name = "cep-service")
    public Cep buscarCep(@SpanTag("cep-value") String cep){
        return CepDTOMapper.fromCepDTO(cepClient.buscarCep(cep));
    }
}
