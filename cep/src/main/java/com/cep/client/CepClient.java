package com.cep.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/ws")
public interface CepClient {

    @NewSpan(name = "cep-client")
    @GetMapping("/{cep}/json")
    CepDTO buscarCep(@SpanTag("cep-value") @PathVariable String cep);
}
