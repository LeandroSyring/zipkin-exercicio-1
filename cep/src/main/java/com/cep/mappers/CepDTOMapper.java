package com.cep.mappers;

import com.cep.client.CepDTO;
import com.cep.models.Cep;

public class CepDTOMapper {

    public static Cep fromCepDTO(CepDTO cepDTO){
        Cep cep = new Cep();
        cep.setCep(cepDTO.getCep());
        cep.setLogradouro(cepDTO.getLogradouro());
        cep.setComplemento(cepDTO.getComplemento());
        cep.setBairro(cepDTO.getBairro());
        cep.setLocalidade(cepDTO.getLocalidade());
        cep.setUf(cepDTO.getUf());
        cep.setUnidade(cepDTO.getUnidade());
        cep.setIbge(cepDTO.getIbge());
        cep.setGia(cepDTO.getGia());

        return cep;
    }
}
