package com.cep.controllers;

import com.cep.models.Cep;
import com.cep.services.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;

@RestController
@RequestMapping("/cep")
public class CepController {

    @Autowired
    private CepService cepService;

    @NewSpan(name = "cep-controller")
    @GetMapping("/{cep}/json")
    public ResponseEntity<Cep> buscarCep(@SpanTag("cep-value") @PathVariable String cep){
        Cep cepReturn = cepService.buscarCep(cep);
        return ResponseEntity.status(200).body(cepReturn);
    }
}
